import requests
import time
import os

# 使用说明: 按需修改这些参数后直接run即可无需改代码
HOST = 'http://127.0.0.1:8888'
REQUEST_URL = '/upload'

# 左边为key
FILES = [
    ('uploadfile', r'E:\漫界\b轨迹3.jpg')
]

HEADERS = {
    "method": "POST",
    "version": "HTTP/1.1",
    "accept": "*/*",
    "accept-encoding": "gzip, deflate",
    "accept-language": "en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4",
    "content-type": "application/x-www-form-urlencoded;charset=UTF-8",
    "User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.1.6)"
}


if __name__ == '__main__':
    conn = requests.session()
    multi_files = list(map(lambda x: (x[0], (os.path.split(x[1])[1], open(x[1], 'rb'))), FILES))
    response = conn.post("%s%s" % (HOST, REQUEST_URL), files=multi_files)
    print(response.text)
