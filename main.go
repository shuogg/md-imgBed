package main

import (
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"strconv"
)

var port = 8888

func main() {
	r := mux.NewRouter()
	r.PathPrefix("/file/").Handler(http.StripPrefix("/file/", http.FileServer(http.Dir("file"))))
	r.HandleFunc("/upload", UploadHandler).Methods("POST")
	err := http.ListenAndServe("0.0.0.0:"+strconv.Itoa(port), r)
	if err != nil {
		log.Fatal("ListenAndServe error: ", err)
	}
}
